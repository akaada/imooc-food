package com.imooc.food.deliverymanservicemanager.enummeration;

public enum DeliverymanStatus {
    AVAILABLE,
    NOT_AVAILABLE;
}
