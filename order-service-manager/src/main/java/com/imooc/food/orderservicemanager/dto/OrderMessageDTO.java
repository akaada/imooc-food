package com.imooc.food.orderservicemanager.dto;

import com.imooc.food.orderservicemanager.enummeration.OrderStatus;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@Setter
@ToString
public class OrderMessageDTO {
    private Integer orderId;
    private OrderStatus orderStatus;
    private BigDecimal price;
    /**
     * 骑手id
     */
    private Integer deliverymanId;
    private Integer productId;
    private Integer accountId;
    /**
     * 结算id
     */
    private Integer settlementId;
    /**
     * 积分结算id
     */
    private Integer rewardId;
    /**
     * 积分奖励数量
     */
    private BigDecimal rewardAmount;
    /**
     * 商家确认
     */
    private Boolean confirmed;
}
