package com.imooc.food.orderservicemanager.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class OrderCreateVO {
    /**
     * 用户ID
     */
    private Integer accountId;

    /**
     * 地址
     */
    private String address;

    /**
     * 产品ID
     */
    private Integer productId;
}
