package com.imooc.food.restaurantservicemanager.enummeration;

public enum  ProductStatus {
    AVAILABLE,
    NOT_AVAILABLE;
}
